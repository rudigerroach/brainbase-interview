# StockTracker

## Overview
This project exists as a coding exercise for [Brainbase](https://www.brainbase.com/).
![Screenshot](./screenshots/img.png "Stock Price simulation")

The goal of the website is to retrieve a list of base prices from a remote server and allow a user to simulate price changes as well as visualise these price changes over time.
The initial stock value can be seen when hovering over the chart on each stock card.

Price information is stored in the browser's localstorage until the user opts to restart the simulation. In a real-life scenario this would help keep load off of the server. 
## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

I did not spend much time on tests due to time constraints during completion of the exercise. Instead, I created only a single test file to showcase how I write tests (offline-store.service.spec.ts). 
In production code I aim for much higher overall test coverage.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
