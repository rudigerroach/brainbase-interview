import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {StockHistory} from '../data-store/stock-history.model';
import {StockValueSnapshot} from '../data-store/stock-value-snapshot.model';
import {faSortUp, faSortDown, IconDefinition} from '@fortawesome/free-solid-svg-icons';

/**
 * These imports are not used in the typescript file but they are required for the chart to render:
 * BrowserModule, NgxChartsModule
 */
// noinspection ES6UnusedImports
import {BrowserModule} from '@angular/platform-browser';
// noinspection ES6UnusedImports
import {NgxChartsModule} from '@swimlane/ngx-charts';

@Component({
  selector: 'stock-card',
  templateUrl: './stock-card.component.html',
  styleUrls: ['./stock-card.component.css']
})
export class StockCardComponent implements OnInit, OnChanges {
  @Input() stock: StockHistory = new StockHistory();
  @Input() effectiveDate: Date = new Date();
  price: number = 0;
  absoluteChange: number = 0;
  percentageChange: number = 0;
  effectiveHistory: StockValueSnapshot[] = [];

  chartData: any[];
  colorScheme = 'forest';

  changeIndicatorIcon: IconDefinition;

  constructor() {
  }

  ngOnInit(): void {
    this.initCard();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initCard();
  }

  private initCard(): void {
    this.effectiveHistory = this.stock.values.filter(value => value.isEffectiveBeforeDay(this.effectiveDate))
    const latestValue = this.effectiveHistory[this.effectiveHistory.length - 1];
    const previousValue = this.getPreviousValue();

    if (!(latestValue && previousValue)) {
      return;
    }

    this.price = latestValue.price;
    this.absoluteChange = latestValue.price - previousValue.price;
    this.percentageChange = (latestValue.price - previousValue.price) / previousValue.price;

    this.setChangeIndicatorIcon();
    this.populateChartData();
  }

  private populateChartData(): void {
    const chartValues = this.effectiveHistory.map(entry => {
      return {'name': entry.effectiveDate, 'value': entry.price}
    });
    this.chartData = [{
      'name': this.stock.symbol,
      'series': chartValues
    }];
  }

  private setChangeIndicatorIcon(): void {
    if (this.absoluteChange < 0) {
      this.changeIndicatorIcon = faSortDown;
    } else {
      this.changeIndicatorIcon = faSortUp;
    }
  }

  private getPreviousValue(): StockValueSnapshot {
    if (this.effectiveHistory.length === 1) {
      return this.effectiveHistory[0];
    }

    return this.effectiveHistory[this.effectiveHistory.length - 2];
  }
}
