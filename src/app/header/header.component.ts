import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {DateTime} from 'luxon';
import {faAngleLeft, faAngleRight, IconDefinition} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnChanges {
  @Input() simulationStartDate: Date = new Date();
  @Output() dateChangeEvent = new EventEmitter<Date>();

  startDate: Date = new Date();
  effectiveDate: Date;
  displayDay: number;

  leftPointerIcon: IconDefinition = faAngleLeft;
  rightPointerIcon: IconDefinition = faAngleRight;

  changeDetector: ChangeDetectorRef;

  constructor(changeDetector: ChangeDetectorRef) {
    this.changeDetector = changeDetector;
    this.initHeader();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initHeader();
  }

  initHeader(): void {
    const now = new Date();
    this.effectiveDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    this.startDate = new Date(this.simulationStartDate.getFullYear(), this.simulationStartDate.getMonth(), this.simulationStartDate.getDate());
    this.displayDay = this.getDisplayDay();
  }

  setNextDay(): void {
    this.updateEffectiveDate(1);
    this.displayDay = this.getDisplayDay();
    this.dateChangeEvent.emit(this.effectiveDate);
  }

  setPreviousDay(): void {
    if (this.displayDay === 1) {
      return
    }
    this.updateEffectiveDate(-1);
    this.displayDay = this.getDisplayDay();
    this.dateChangeEvent.emit(this.effectiveDate);
  }

  updateEffectiveDate(daysDifference: number): void {
    /**
     * Because we are using setDate to update the date, change detection doe not pick up the change
     * This is a little workaround to ensure change detection occurs. There is probably a more elegant way
     * out there to get to the same result.
     */
    const temporaryDate = this.effectiveDate;
    this.effectiveDate = null;
    this.changeDetector.detectChanges();
    this.effectiveDate = temporaryDate;
    this.effectiveDate.setDate(this.effectiveDate.getDate() + daysDifference);
    this.changeDetector.detectChanges();
  }

  getDisplayDay(): number {
    const effectiveDate = DateTime.fromISO(this.effectiveDate.toISOString());
    const simulationStartDate = DateTime.fromISO(this.startDate.toISOString());
    const daysDifference: number = effectiveDate.diff(simulationStartDate, ['days'])['days']
    return daysDifference + 1
  }
}
