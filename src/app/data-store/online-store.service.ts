import {StockHistory} from './stock-history.model';
import {Stock} from '../stocks.model';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StockValueSnapshot} from './stock-value-snapshot.model';

@Injectable({
  providedIn: 'root'
})
export class OnlineStore {
  STOCKS_API_URL = '/api/stocks.php';

  constructor(private http: HttpClient) {
  }

  async getAll(effectiveDate: Date): Promise<StockHistory[]> {
    let stocks = await this.fetchStocksFromRemote()

    return stocks.map(stock => {
      const stockHistory = new StockHistory(stock.name, stock.symbol)
      const valueSnapshot = new StockValueSnapshot(effectiveDate, stock.price);
      stockHistory.values = [valueSnapshot];

      return stockHistory;
    });
  }

  private async fetchStocksFromRemote(): Promise<Stock[]> {
    return await this.http.get<Stock[]>(this.STOCKS_API_URL).toPromise();
  }
}
