import {StockHistory} from './stock-history.model';
import {Injectable} from '@angular/core';
import {StockValueSnapshot} from './stock-value-snapshot.model';

@Injectable({
  providedIn: 'root'
})
export class OfflineStore {

  constructor() {
  }

  store(stockHistory: StockHistory[]): void {
    const historyAsString = JSON.stringify(stockHistory)
    localStorage.setItem(StorageKeys.STOCKS, historyAsString);
  }

  isEmpty(): boolean {
    return !!!localStorage.getItem(StorageKeys.STOCKS);
  }

  getStocks(): StockHistory[] {
    const stocks: string = localStorage.getItem(StorageKeys.STOCKS);
    if (!stocks) {
      return [];
    }

    const jsonStocks = <Array<any>>JSON.parse(stocks);
    return jsonStocks.map(entry => {
      const historyEntry = new StockHistory(entry['name'], entry['symbol'])
      for (let valueSnapshot of entry['values']) {
        const value = new StockValueSnapshot(new Date(valueSnapshot['effectiveDate']), valueSnapshot['price']);
        historyEntry.values.push(value);
      }
      return historyEntry;
    });
  }

  reset(): void {
    localStorage.removeItem(StorageKeys.START_DATE);
    localStorage.removeItem(StorageKeys.UPDATE_DATE);
    localStorage.removeItem(StorageKeys.STOCKS);
  }

  getStartDate(): Date {
    return this.getDate(StorageKeys.START_DATE);
  }

  getUpdateDate(): Date {
    return this.getDate(StorageKeys.UPDATE_DATE);
  }

  storeStartDate(date: Date): void {
    this.setDate(date, StorageKeys.START_DATE);
  }

  storeUpdateDate(date: Date): void {
    this.setDate(date, StorageKeys.UPDATE_DATE);
  }

  private getDate(key: string): Date {
    const dateString: string = localStorage.getItem(key);
    if (!dateString) {
      return null;
    }
    return new Date(dateString);
  }

  private setDate(date: Date, key: string): void {
    localStorage.setItem(key, date.toISOString());
  }

}

enum StorageKeys {
  STOCKS = 'stocks',
  START_DATE = 'startDate',
  UPDATE_DATE = 'updateDate'
}
