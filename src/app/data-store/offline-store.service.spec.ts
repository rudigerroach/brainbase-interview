import { TestBed } from '@angular/core/testing';

import { OfflineStore } from './offline-store.service';
import {StockHistory} from './stock-history.model';
import {StockValueSnapshot} from './stock-value-snapshot.model';

describe('OfflineStore', () => {
  let service: OfflineStore;
  let localStorage: any;

  const exampleStockHistory = new StockHistory('Apple', 'AAPL');
  const exampleValue = new StockValueSnapshot(new Date(), 1);
  exampleStockHistory.values.push(exampleValue);

  beforeEach(() => {
    TestBed.configureTestingModule({});
    localStorage = {};
    service = new OfflineStore();

    spyOn(window.localStorage, 'getItem').and.callFake(function (key) {
      return localStorage[key];
    });
    spyOn(window.localStorage, 'setItem').and.callFake(function (key, value) {
      return localStorage[key] = value + '';
    });
    spyOn(window.localStorage, 'removeItem').and.callFake(function (key) {
      delete localStorage[key];
    });
  });

  it('should store and retrieve stock history objects from the local store', () => {
    const stockHistory = [exampleStockHistory];
    service.store(stockHistory);

    const retrievedEntry: StockHistory[] = service.getStocks();

    expect(retrievedEntry.length).toEqual(1)
    expect(retrievedEntry[0].name).toEqual('Apple');
    expect(retrievedEntry[0].symbol).toEqual('AAPL');
    expect(retrievedEntry[0].values.length).toEqual(1);
    expect(retrievedEntry[0].values[0].price).toEqual(exampleValue.price);
    expect(retrievedEntry[0].values[0].effectiveDate).toEqual(exampleValue.effectiveDate);
  });

  it('should be able to get and set start dates',() => {
    const startDate = new Date();
    service.storeStartDate(startDate);

    const retrievedDate = service.getStartDate();
    expect(retrievedDate).toEqual(startDate)
  });
  it('should be able to get and set update dates',() => {
    const updateDate = new Date();
    service.storeUpdateDate(updateDate);

    const retrievedDate = service.getUpdateDate();
    expect(retrievedDate).toEqual(updateDate)
  });

  describe('getStocks', () => {
    it('should return an empty array if localstorage is empty and get', () => {
      const retrievedEntry: StockHistory[] = service.getStocks();
      expect(retrievedEntry.length).toEqual(0);
    });
  });

  describe('isEmpty', () => {
    it('should return true if localstorage is empty of stocks', () => {
      const isEmpty = service.isEmpty();
      expect(isEmpty).toBeTruthy();
    });
    it('should return false if localstorage is not empty of stocks', () => {
      service.store([exampleStockHistory])
      const isEmpty = service.isEmpty();
      expect(isEmpty).toBeFalse();
    });
  });

  describe('reset', () => {
    it('should remove all data from local storage', () => {
      service.store([exampleStockHistory])
      service.storeStartDate(new Date());
      service.storeUpdateDate(new Date());
      expect(Object.keys(localStorage).length).toEqual(3);

      service.reset()
      expect(Object.keys(localStorage).length).toEqual(0);
    });
  });

});
