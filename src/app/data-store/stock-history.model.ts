import {StockValueSnapshot} from './stock-value-snapshot.model';
import {DateTime, Duration} from 'luxon';

export class StockHistory {
  name: string;
  symbol: string;
  values: Array<StockValueSnapshot>;

  constructor(name?: string, symbol?: string) {
    this.name = name;
    this.symbol = symbol;
    this.values = [];
  }

  get lastValue(): StockValueSnapshot | null {
    if (this.values.length > 0) {
      return this.values[this.values.length - 1];
    }
    return null;
  }

  hasValueAtDate(date: Date): boolean {
    const lastValueDate = DateTime.fromJSDate(this.lastValue.effectiveDate);
    const searchDate = DateTime.fromJSDate(date);

    return searchDate <= lastValueDate;
  }

  addNextValue(newStockValue: number): void {
    const lastDate = DateTime.fromJSDate(this.lastValue.effectiveDate);
    const oneDay = Duration.fromObject({days: 1});
    const nextDay = lastDate.plus(oneDay);

    const newValue = new StockValueSnapshot();
    newValue.effectiveDate = nextDay.toJSDate();
    newValue.price = newStockValue;

    this.values.push(newValue);
  }
}
