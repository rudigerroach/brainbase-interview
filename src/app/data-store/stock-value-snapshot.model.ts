export class StockValueSnapshot {
  effectiveDate: Date;
  price: number;

  constructor(effectiveDate?: Date, price?: number) {
    this.effectiveDate = effectiveDate;
    this.price = price;
  }

  isEffectiveBeforeDay(date: Date): boolean {
    const otherDay = new Date(date.getFullYear(), date.getMonth(), date.getDate())
    const thisDay = new Date(this.effectiveDate.getFullYear(), this.effectiveDate.getMonth(), this.effectiveDate.getDate())
    return thisDay.getTime() <= otherDay.getTime();
  }
}
