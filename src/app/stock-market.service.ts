import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {OnlineStore} from './data-store/online-store.service';
import {OfflineStore} from './data-store/offline-store.service';
import {StockHistory} from './data-store/stock-history.model';

@Injectable({
  providedIn: 'root'
})
export class StockMarketService {

  private startDate: Date;
  private lastUpdatedAt: Date;
  private stocks: Array<StockHistory> = [];

  constructor(
    private http: HttpClient,
    private onlineStore: OnlineStore,
    private offlineStore: OfflineStore
  ) {
  }

  async initialise(): Promise<void> {
    if (this.offlineStore.isEmpty()) {
      const currentDate = new Date();
      const onlineEntries: StockHistory[] = await this.onlineStore.getAll(currentDate);
      this.offlineStore.store(onlineEntries);

      this.startDate = currentDate;
      this.offlineStore.storeStartDate(this.startDate);
    }

    this.stocks = this.offlineStore.getStocks();
    this.startDate = this.startDate || this.offlineStore.getStartDate();
  }

  getStocks(): Array<StockHistory> {
    return this.stocks;
  }

  getStartDate(): Date | null {
    return this.startDate;
  }

  isUpToDate(effectiveDate: Date): boolean {
    const lastUpdatedAt = this.offlineStore.getUpdateDate();
    return lastUpdatedAt && (effectiveDate.getTime() <= lastUpdatedAt.getTime());
  }

  updatePrices(effectiveDate: Date): void {
    for (let stock of this.stocks) {
      while (!stock.hasValueAtDate(effectiveDate)) {
        const newStockValue: number = this.generateRandomStockValue(stock.lastValue.price);
        stock.addNextValue(newStockValue);
      }
    }
    this.offlineStore.store(this.stocks);
    this.lastUpdatedAt = effectiveDate;
    this.offlineStore.storeUpdateDate(this.lastUpdatedAt);
  }

  async reset(): Promise<void> {
    this.offlineStore.reset();
    await this.initialise();
  }

  private generateRandomStockValue(lastValue: number): number {
    const randomChange: number = this.getPercentageChange();
    if (this.randomiseToPositiveChange()) {
      return lastValue + (lastValue * randomChange);
    }
    return lastValue - (lastValue * randomChange);
  }

  private randomiseToPositiveChange(): boolean {
    return Math.round(Math.random()) === 1;
  }

  /**
   * return a number between zero and 0.1
   * that can be used as a multiplier where the change should be less than or equal to 10 percent
   * @private
   */
  private getPercentageChange(): number {
    return Math.random() / 10;
  }
}
