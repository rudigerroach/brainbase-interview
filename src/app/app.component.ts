import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {StockMarketService} from './stock-market.service';
import {StockHistory} from './data-store/stock-history.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  changeDetector: ChangeDetectorRef;
  stockMarket: StockMarketService;
  stocks: StockHistory[] = [];
  simulationStartDate: Date = new Date();
  effectiveDate: Date = new Date();
  title = 'stock-tracker';

  constructor(stockMarket: StockMarketService, changeDetector: ChangeDetectorRef) {
    this.stockMarket = stockMarket;
    this.changeDetector = changeDetector;
  }

  async ngOnInit(): Promise<void> {
    await this.stockMarket.initialise();
    this.simulationStartDate = this.stockMarket.getStartDate();
    this.stocks = this.stockMarket.getStocks();
  }

  setDate(effectiveDate: Date): void {
    this.effectiveDate = effectiveDate;
    if (!this.stockMarket.isUpToDate(effectiveDate)) {
      this.stockMarket.updatePrices(effectiveDate);
      this.stocks = this.stockMarket.getStocks();
    }
    this.forceChangeDetection();
  }

  async resetSimulation(): Promise<void> {
    await this.stockMarket.reset();
    this.effectiveDate = new Date();
    this.simulationStartDate = new Date();
    this.stocks = this.stockMarket.getStocks();
  }

  private forceChangeDetection(): void {
    this.stocks = [];
    this.changeDetector.detectChanges();
    this.stocks = this.stockMarket.getStocks();
    this.changeDetector.detectChanges();
  }
}


